package com.vinith.androidproficiency.api

import com.vinith.androidproficiency.model.FactsEntity
import retrofit2.Call
import retrofit2.http.GET

interface ListApi {
    companion object {
        const val HOSTNAME = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/"
    }

    @GET("facts.json")
    fun getFacts(): Call<FactsEntity>
}