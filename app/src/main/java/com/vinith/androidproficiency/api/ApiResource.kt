package com.vinith.androidproficiency.api

data class ApiResource<out T>(val status:Status, val data:T?, val message:String?) {
    companion object {
        fun <T> success(data: T?): ApiResource<T> {
            return ApiResource(Status.SUCCESS, data, null)
        }

        fun <T> error(msg:String, data: T?): ApiResource<T> {
            return ApiResource(Status.ERROR, data, msg)
        }
    }
}

enum class Status {SUCCESS, ERROR}