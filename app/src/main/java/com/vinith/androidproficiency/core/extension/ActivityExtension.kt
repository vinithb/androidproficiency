package com.vinith.androidproficiency.core.extension

import androidx.appcompat.app.AppCompatActivity
import com.vinith.androidproficiency.core.base.BaseFragment

fun AppCompatActivity.loadFragment(fragment: BaseFragment, containerId: Int) {
    supportFragmentManager.beginTransaction().add(containerId, fragment)
        .commit()
}

