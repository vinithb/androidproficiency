package com.vinith.androidproficiency.core.build

class BuildDevelop : BuildConstantBase() {

    override fun isDevelopBuild(): Boolean = true

    override fun isLogEnabled(): Boolean = true
}