package com.vinith.androidproficiency.core.build

import com.vinith.androidproficiency.BuildConfig

class BuildConstant {
    companion object {
        lateinit var buildConstantBase: BuildConstantBase

        init {
            when(BuildConfig.BUILD_TYPE) {
                "debug" -> buildConstantBase = BuildDevelop()
                "release" -> buildConstantBase = BuildRelease()
            }
        }
    }
}