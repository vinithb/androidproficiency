package com.vinith.androidproficiency.core.dagger

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.vinith.androidproficiency.feature.listfacts.ListFactViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ListFactViewModel::class)
    abstract fun bindListFactViewModel(listFactViewModel: ListFactViewModel): ViewModel
}