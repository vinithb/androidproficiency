package com.vinith.androidproficiency.core.dagger

import com.vinith.androidproficiency.core.dagger.api.ApiModule
import com.vinith.androidproficiency.feature.listfacts.ListFactFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApiModule::class, ViewModelFactoryModule::class])
interface AppComponent {
    fun inject(listFactFragment: ListFactFragment)
}