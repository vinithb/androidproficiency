package com.vinith.androidproficiency.core.extension

import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.vinith.androidproficiency.R

fun ImageView.loadImage(imageUrl: String) {
    Picasso.get()
        .load(Uri.parse(imageUrl))
        .placeholder(R.drawable.placeholder)
        .error(R.drawable.placeholder)
        .into(this)
}