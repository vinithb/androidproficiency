package com.vinith.androidproficiency.core.dagger.api

data class BaseUrlHolder(var baseUrl: String)