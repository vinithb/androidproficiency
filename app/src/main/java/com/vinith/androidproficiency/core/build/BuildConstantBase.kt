package com.vinith.androidproficiency.core.build

import com.vinith.androidproficiency.api.ListApi

abstract class BuildConstantBase {

    open fun getBaseApi(): String  = ListApi.HOSTNAME

    open fun isLogEnabled(): Boolean = false

    open fun isDevelopBuild(): Boolean = false

    open fun isReleaseBuild(): Boolean = false

}