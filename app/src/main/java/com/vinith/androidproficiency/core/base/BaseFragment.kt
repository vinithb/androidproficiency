package com.vinith.androidproficiency.core.base

import androidx.fragment.app.Fragment

open class BaseFragment: Fragment() {
    interface NetworkAvailable {
        fun onNetworkAvailable(isAvailable: Boolean)
    }
}