package com.vinith.androidproficiency.feature.listfacts

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vinith.androidproficiency.R
import com.vinith.androidproficiency.core.extension.loadImage
import kotlinx.android.synthetic.main.layout_list_item.view.*

class ListFactAdapter(private var factList: ArrayList<ListFactView>) :
    RecyclerView.Adapter<ListFactAdapter.FactHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): FactHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.layout_list_item, parent, false)
        return FactHolder(inflatedView)
    }

    override fun onBindViewHolder(holder: FactHolder, position: Int) {
        holder.bindFactHolder(factList[position])
    }

    override fun getItemCount(): Int {
        return factList?.size
    }

    fun setFactList(newFactList: ArrayList<ListFactView>) {
        factList = newFactList
    }

    class FactHolder(v: View) :
        RecyclerView.ViewHolder(v) {

        private var view: View = v

        fun bindFactHolder(fact: ListFactView) {
            view.titleTV.text = fact.title
            view.descTV.text = fact.desc
            view.imageView.loadImage(fact.image)
        }
    }
}