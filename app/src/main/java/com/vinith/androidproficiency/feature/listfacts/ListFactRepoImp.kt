package com.vinith.androidproficiency.feature.listfacts

import com.vinith.androidproficiency.api.ApiResource
import com.vinith.androidproficiency.api.ListApi
import com.vinith.androidproficiency.model.FactsEntity
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Response
import javax.inject.Inject

class ListFactRepoImp @Inject constructor() : ListFactRepository {

    val successResponse = 200

    @Inject
    lateinit var api: ListApi

    /**
     * Call API using retrofit asynchronous request
     */
    override suspend fun getFact(
        successHandler: (ApiResource<FactsEntity>?) -> Unit,
        failureHandler: (ApiResource<FactsEntity>?) -> Unit
    ) {
        withContext(Dispatchers.IO) {
            api.getFacts().enqueue(object : retrofit2.Callback<FactsEntity> {
                override fun onResponse(call: Call<FactsEntity>, response: Response<FactsEntity>) {
                    if (response.code() == successResponse) {
                        successHandler(ApiResource.success(response.body()))
                    } else {
                        failureHandler(ApiResource.error(response.code().toString(), null))
                    }
                }

                override fun onFailure(call: Call<FactsEntity>, t: Throwable) {
                    failureHandler(ApiResource.error(t.localizedMessage.toString(), null))
                }
            })
        }
    }
}