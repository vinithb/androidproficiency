package com.vinith.androidproficiency.feature.listfacts

import android.os.Bundle
import com.vinith.androidproficiency.R
import com.vinith.androidproficiency.core.base.BaseActivity
import com.vinith.androidproficiency.core.extension.loadFragment

class ListFactActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_listfact)
        loadFragment(ListFactFragment.newInstance(), R.id.container)
    }

    /**
     * Set action bar title
     */
    fun setActionBarTitle(newTitle: String) {
        if (newTitle.isNotEmpty()) {
            title = newTitle
        }
    }
}