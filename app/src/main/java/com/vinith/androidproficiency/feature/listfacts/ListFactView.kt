package com.vinith.androidproficiency.feature.listfacts

data class ListFactView(var title:String, var desc:String, var image:String, var actionBarTitle: String) {
    companion object {
        fun empty() = ListFactView("","", "","")
    }
}