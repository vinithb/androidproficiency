package com.vinith.androidproficiency.feature.listfacts

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.vinith.androidproficiency.ListApp
import com.vinith.androidproficiency.R
import com.vinith.androidproficiency.core.base.BaseFragment
import com.vinith.androidproficiency.core.dagger.AppViewModelFactory
import com.vinith.androidproficiency.core.extension.isNetworkAvailable
import com.vinith.androidproficiency.core.extension.registerNetworkCallback
import com.vinith.androidproficiency.core.extension.showToastMsgSmall
import kotlinx.android.synthetic.main.fragment_list_fact.*
import javax.inject.Inject

class ListFactFragment : BaseFragment() {

    private lateinit var viewModel: ListFactViewModel
    private var adapter: ListFactAdapter? = null

    @Inject
    lateinit var viewModelFactory: AppViewModelFactory

    companion object {
        fun newInstance() = ListFactFragment().apply {
            arguments = Bundle().apply { }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity?.applicationContext as ListApp).appComponent.inject(this)

        viewModel = activity?.let {
            ViewModelProvider(it, viewModelFactory).get(ListFactViewModel::class.java)
        }!!
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list_fact, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        swipeContainer.setOnRefreshListener {
            getFact(false)
        }
        attachObserver()
        getFact(true)

        registerNetworkCallback(object : NetworkAvailable {
            override fun onNetworkAvailable(isAvailable: Boolean) {
                activity?.runOnUiThread { getFact(true)}
            }
        })
    }

    private fun getFact(showProgress: Boolean) {
        if (isNetworkAvailable()) {
            if (showProgress)
                pgBar.visibility = View.VISIBLE

            viewModel.getFact()
        } else {
            pgBar.visibility = View.GONE
            swipeContainer.isRefreshing = false
            showToastMsgSmall(getString(R.string.network_unavailable))
        }
    }

    private fun attachObserver() {
        viewModel.factView.observe(viewLifecycleOwner, {
            swipeContainer.isRefreshing = false
            pgBar.visibility = View.GONE
            updateListAdapter(it)
        })

        viewModel.actionBarTitle.observe(viewLifecycleOwner, {
            (activity as ListFactActivity).setActionBarTitle(it)
        })

        viewModel.apiError.observe(viewLifecycleOwner, {
            swipeContainer.isRefreshing = false
            pgBar.visibility = View.GONE
            if (it) {
                showToastMsgSmall(getString(R.string.api_error))
            }
        })
    }

    private fun updateListAdapter(list: ArrayList<ListFactView>) {
        if (adapter == null) {
            adapter = ListFactAdapter(list)
            val linearLayoutManager = LinearLayoutManager(activity)
            rvItems.layoutManager = linearLayoutManager
            rvItems.adapter = adapter
        } else {
            adapter!!.setFactList(list)
        }

        adapter?.notifyDataSetChanged()
    }

}