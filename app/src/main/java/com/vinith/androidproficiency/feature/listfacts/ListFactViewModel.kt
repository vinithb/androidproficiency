package com.vinith.androidproficiency.feature.listfacts

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.vinith.androidproficiency.api.ApiResource
import com.vinith.androidproficiency.core.base.BaseViewModel
import com.vinith.androidproficiency.model.FactsEntity
import kotlinx.coroutines.launch
import javax.inject.Inject

class ListFactViewModel
    @Inject constructor(private var repository: ListFactRepoImp): BaseViewModel() {

    var factView = MutableLiveData<ArrayList<ListFactView>>()
    var actionBarTitle = MutableLiveData<String>()
    var apiError = MutableLiveData<Boolean>()

    /**
     * Calls fact api via repository
     * Updates live data based on response
     */
    fun getFact() {
        viewModelScope.launch {
            repository.getFact({
                if(it?.data != null) {
                    onFactsReceived(it)
                } else {
                    apiError.value = true
                }
            }, {
                apiError.value = true
            })
        }
    }

    private fun onFactsReceived(it: ApiResource<FactsEntity>) {
        actionBarTitle.value = it.data?.title
        factView.value = it.data?.let { it1 -> getFactView(it1) }
    }

    /**
     * Converts entity class to presentable view
     */
    private fun getFactView(factsEntity: FactsEntity): ArrayList<ListFactView> {
        val list = ArrayList<ListFactView>()
        for(rows in factsEntity.rows) {
            val factView = ListFactView.empty()
            factView.title = rows.title ?: "Title"
            factView.desc = rows.description ?: "Description"
            factView.image = rows.imageHref ?: ""
            list.add(factView)
        }

        return list
    }
}