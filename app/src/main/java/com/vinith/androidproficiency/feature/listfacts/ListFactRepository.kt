package com.vinith.androidproficiency.feature.listfacts

import com.vinith.androidproficiency.api.ApiResource
import com.vinith.androidproficiency.model.FactsEntity

interface ListFactRepository {
    suspend fun getFact(
        successHandler: (ApiResource<FactsEntity>?) -> Unit,
        failureHandler: (ApiResource<FactsEntity>?) -> Unit
    )
}