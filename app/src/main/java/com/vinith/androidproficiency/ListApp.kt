package com.vinith.androidproficiency

import android.app.Application
import com.vinith.androidproficiency.core.dagger.AppComponent
import com.vinith.androidproficiency.core.dagger.DaggerAppComponent

class ListApp: Application() {
    val appComponent: AppComponent = DaggerAppComponent.create()
}