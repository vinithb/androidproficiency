package com.vinith.androidproficiency.model

data class Row(
    val description: String,
    val imageHref: String,
    val title: String
)