package com.vinith.androidproficiency.model

data class FactsEntity(
    val rows: List<Row>,
    val title: String
)